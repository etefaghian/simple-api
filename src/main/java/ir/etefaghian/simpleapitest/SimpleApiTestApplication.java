package ir.etefaghian.simpleapitest;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SimpleApiTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimpleApiTestApplication.class, args);
    }

}
